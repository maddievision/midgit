import sys
import pyIT
import yaml
from MIDI import MIDIFile

def get_fx_name(modnote):
  if modnote.Effect == None: return ''
  return chr(ord("A")+modnote.Effect - 1)

def mod_convert(modfile,trkprefix,destfile,
  channels=[],dests=[],instruments=[],transpose=0,
  startframe="0:0",endframe="-1:-1",
  pitchrange=12,tickrate=24,drumnote=None,
  gxx_overlap=False,hxx_use_cc=None,scx_adjust=0,
  note_vol_cc=None,max_note_len=None,def_vel=100,reset_pitch_on_note_off=True,
  v00_note_off=False,v00_min=0,zxx_use_cc=None
  ):

    visitedframes = []

    if destfile == None:
      destfile = modfile + ".mid"

    mdl = pyIT.ITfile()
    mdl.open(modfile)
    if len(channels) == 0:
      channels += xrange(64)
    else:
      channels = [c-1 for c in channels]
    if len(dests) == 0:
      dests = [0] * len(channels)
    else:
      dests = [d-1 for d in dests]

    def destforch(chn):
      if chn not in channels:
        return -1
      else:
        return dests[channels.index(chn)]

    events = [[] for x in xrange(max(dests) + 1)]

    if len(instruments) == 0:
      instruments += xrange(1,len(mdl.Instruments) + 1)
    sframe = tuple([int(denis) for denis in startframe.split(":")])
    eframe = tuple([int(denis) for denis in endframe.split(":")])
    if eframe[0] == -1: eframe = (len(mdl.Orders) - 2, eframe[1])
    if eframe[1] == -1: eframe = (eframe[0],len(mdl.Patterns[mdl.Orders[eframe[0]]].Rows) - 1)
    print "Converting %s => %s: Channels %s, Dests %s, Instruments %s, Frames %s -> %s" % (modfile,destfile,[c+1 for c in channels],[d+1 for d in dests],instruments,sframe,eframe)
    tick = 0
    wearedone = False
    sbloop = False
    sblooprow = -1
    sbcount = -1
    speed = mdl.IS
    rowdelay = 0
  
    class ChInfoSet(object):
      def __init__(self):
        self.pitchoff = 0
        self.curmidinote =-1
        self.vibstatus = 0
        self.vibpitch = 0
        self.lastinst = -1
        self.lastnote = None
        self.curnote = -1
        self.targnote = -1
        self.notevol = 64
        self.midinotevol = -1
        self.lastmidinotevol = -1
        self.fxmem = {}

    chinfo = {}
    for x in xrange(64):
      chinfo[x] = ChInfoSet()


    def getvolcmd(n):
      nfx = ''
      nfxarg = 0
      val = n.Volume
      if val != None:
        if val <= 64:
          nfx = 'v'
          nfxarg = val
        elif val <= 74:
          pass         
        elif val <= 84:
          pass         

      return (nfx,nfxarg)

    def getfxmem(ch,fxname):
      if fxname in ('F','G'): fxname = 'E'
      if fxname in chinfo[ch].fxmem:
        return chinfo[ch].fxmem[fxname]
      else:
        return -1
    def setfxmem(ch,fxname,val):
      if fxname in ('F','G'): fxname = 'E'
      chinfo[ch].fxmem[fxname] = val

    def fxmemzero(ch,note):
      if note.EffectArg == 0:
        return getfxmem(ch,get_fx_name(note))
      else:
        return note.EffectArg

    def docc(ch,cc,val,tick):
      events[destforch(ch)].append({"type": "cc", "cc": cc, "val": val, "tick": tick})
    def dopitchbend(ch,val,tick):
      events[destforch(ch)].append({'type':'pitch','val':val,'tick':tick})

    def startnewnote(ch,nn,tick,vel):
      chinfo[ch].lastnote = {"type":"note","nn":nn,"tick":tick,"vel":vel}
      chinfo[ch].curnote = nn * 64
      chinfo[ch].curmidinote = nn * 64
      chinfo[ch].targnote = nn * 64
      if chinfo[ch].pitchoff != 0:
        dopitchbend(ch,0,tick)
        chinfo[ch].pitchoff = 0

    def endnote(ch,tick):
      ln = chinfo[ch].lastnote
      if ln != None:
        ln['duration'] = tick - ln['tick']
        if ln['duration'] > 0 and ln['vel'] > 0:
          if max_note_len != None and ln['duration'] > max_note_len: ln['duration'] = max_note_len
          events[destforch(ch)].append(ln)
        chinfo[ch].lastnote = None

        if reset_pitch_on_note_off:
          if chinfo[ch].pitchoff != 0:
            dopitchbend(ch,0,tick)
            chinfo[ch].pitchoff = 0

    for i,pid in enumerate(mdl.Orders):
      if pid == 255: break
      pat = mdl.Patterns[pid]
      pitchevents = 0
      for j,row in enumerate(pat.Rows):
        thisframe = (i,j)
        thisord,thisrow = thisframe
        if thisframe in visitedframes and not sbloop:
          print "looped or some shit"
          #oh no no no no dont cause infinite loop!
          wearedone = True
          break
        elif thisframe not in visitedframes: visitedframes.append(thisframe)

        rowdelay = 0

        # PRE-SCAN
        for ch,n in enumerate(row):
          fx = get_fx_name(n)
          if fx == "A":
            arg = n.EffectArg
            speed = arg
#           print "we found your dick"
#           print "it was %d" % note.EffectArg
#           print "...... lolllll"
          elif fx == "S":
            arg = fxmemzero(ch,n)
            if arg == 0xB0:
              sblooprow = thisrow
            elif arg >= 0xE0 and arg <= 0xEF:
              rowdelay = arg & 0xF


        # BODY-SCAN
        for ch,n in enumerate(row):
          lane = destforch(ch)
          chi = chinfo[ch]
          if lane == -1: continue #fuck this channel

          fx = get_fx_name(n)
#          if fx in ["G"]: fx = ""
          fxarg = None
          nfx,nfxarg = getvolcmd(n)
          if nfx == 'v':
            chi.notevol = nfxarg

          chi.midinotevol = int(float(chi.notevol) / 64. * 127.)
          if chi.lastmidinotevol != chi.midinotevol:
            if note_vol_cc != None:
              docc(ch,note_vol_cc,chi.midinotevol,tick)
            chi.lastmidinotevol = chi.midinotevol

          if fx != '': fxarg = fxmemzero(ch,n)
          if fx == 'Z' and zxx_use_cc != None:
            nc = int( (float(fxarg) / float(0x7F)) * 127. )
            docc(ch,zxx_use_cc,nc,tick)
          if chi.vibstatus != 0 and fx not in ('H','K','U'):
            chi.vibstatus = 0
            if hxx_use_cc != None:
              docc(ch,hxx_use_cc,0,tick)
          elif chi.vibstatus == 0 and fx in ('H','K','U'):
            chi.vibstatus = fxarg
            if hxx_use_cc != None:
              docc(ch,hxx_use_cc,127,tick)
          ndelay = 0
          nchop = -1
          if fx == 'S':
            hi = fxarg // 0x10
            lo = fxarg & 0xF
            if hi == 0xC:
              nchop = lo + scx_adjust
            elif hi == 0xD:
              ndelay = lo

          if nfx == 'v' and nfxarg <= v00_min and v00_note_off:
            endnote(ch,tick+ndelay)

          if n.Note != None: #note!
            nid = n.Note
            if nid == 254 or nid == 255: #note cut/off
              endnote(ch,tick + ndelay)
            elif nid < 120: #note on
              thisinst = n.Instrument
              if thisinst == None:
                thisinst = chinfo[ch].lastinst
              chi.lastinst = thisinst


              if thisinst in instruments: #deal with it
                vel = def_vel
                if nfx not in ('v','d','e'):
                  chi.notevol = 64
                  chi.midinotevol = int(float(chi.notevol) / 64. * 127.)
                  if chi.lastmidinotevol != chi.midinotevol:
                    if note_vol_cc != None:
                      docc(ch,note_vol_cc,chi.midinotevol,tick)
                    chi.lastmidinotevol = chi.midinotevol

                if note_vol_cc == None:
                  if nfx :
                    vel = chi.midinotevol
                    #print "vel %d from %d" % (vel,n.Volume)

                endtick = tick + ndelay
                if drumnote != None and thisinst in drumnote: nid = drumnote[thisinst]



                if gxx_overlap and fx == 'G': endtick += 1
                if not gxx_overlap and fx =='G' and chi.lastnote != None:
                  chi.targnote = (nid+transpose) * 64
                else:
                  endnote(ch,endtick)
                  startnewnote(ch,nid+transpose,tick + ndelay,vel)

              else:
                endnote(ch,tick + ndelay)
          if nchop > -1:
            endnote(ch,tick + nchop)



          if chi.lastnote != None and fx in ['E','F','G']:
            pitchevents += 1
#            if pitchevents > 1: 
            #print "pitch event %d - %s %d" % (pitchevents, fx, fxarg)
            if not (gxx_overlap and fx == 'G'):

    #          print "hoisin"
              ontick0 = False
              incr = fxarg
              if fx in ('E','F'):
                if incr >= 0xE0:
                  ontick0 = True
                  if incr >= 0xF0:
                    incr = 4 * (incr & 0xF)
                  else:
                    incr = incr & 0xF
                else:
                  incr = 4 * (incr)
              else:
                incr = 4 * (incr)
              tn = chi.targnote 
              if fx == 'E': incr = -incr;
              if fx == 'G' and tn < chi.curnote: incr = -incr
              if fx in ['E','F'] or (chi.curnote != tn and fx == 'G'):
                for mm in xrange(speed):
      #            print "hoisin %d and %s" % (mm,ontick0)
                  if (mm == 0 and ontick0) or (mm > 0 and not ontick0):
    #                print "hoopla"
                    chi.curnote += incr

                    if fx == 'G':
                      #print "gee"
                      if incr > 0 and chi.curnote > tn: chi.curnote = tn
                      elif incr < 0 and chi.curnote < tn: chi.curnote = tn

                    reqoff = float(chi.curnote - chi.curmidinote) / float(64)
                    reqoff = int((float(reqoff) / float(pitchrange)) * 8192)
                    #print reqoff
                    if reqoff > 8192: reqoff = 8191
                    if reqoff < -8192: reqoff = -8192


                    if reqoff != chi.pitchoff:
                      chi.pitchoff = reqoff
                      dopitchbend(ch,reqoff,tick + mm)
          else:
            pitchevents  = 0

        # POST-SCAN
        dobreak = False
        for ch,n in enumerate(row):
          fx = get_fx_name(n)
          if fx != "":
            arg = fxmemzero(ch,n)
            setfxmem(ch,fx,arg)
            if fx == 'C' or fx == 'B':
              dobreak = True



        tick += speed + rowdelay
#        print tick



        if eframe == thisframe and not sbloop:
#          print "end of the road"
          wearedone = True
          break
        if dobreak: break

      if wearedone: break
    for ch in xrange(64):
      if ch in channels:
        endnote(ch,tick)
        dopitchbend(ch,0,0)
        if hxx_use_cc != None: 
          docc(ch,hxx_use_cc,0,0)

    # Create the MIDIFile Object with 1 track
    smf = MIDIFile(len(events))

    for i,x in enumerate(events):
      trkn = "%s %d" % (trkprefix, i+1)
      print "%s has %d events" % (trkn,len(x))

      smf.addTrackName(i,0,trkn)
      # Add track name and tempo.
      #smf.addTempo(track,time,120)

      for y in x:
        #print y
        channel = i
        if y['type'] == "note":
          nn = y['nn']
          time = float(y['tick']) / float(tickrate)
          duration = float(y['duration']) / float(tickrate)
          velo = y['vel']

          # Now add the note.
          smf.addNote(i,channel,nn,time,duration,velo)
        elif y['type'] == "cc":
          cc = y['cc']
          val = y['val']
          time = float(y['tick']) / float(tickrate)
          smf.addControllerEvent(i,channel,time,cc,val)
        elif y['type'] == "pitch":
          pitch = y['val']
          time = float(y['tick']) / float(tickrate)
          smf.addPitch(i,channel,time,pitch)

    # And write it to disk.
    binfile = open(destfile, 'wb')
    smf.writeFile(binfile)
    binfile.close()


if __name__ == "__main__":
    USAGE_STRING = """
    ----------------
    Usage: 
      python midgIT.py <tasks file.yaml> [filter]

      filter is optional comma separated list of tasks, otherwise all tasks are run

    ----------------
    """

    if len(sys.argv) < 2:
      print "You did not specify a file! Please specify a task file!"
      print USAGE_STRING
      sys.exit(1)

    fn = sys.argv[1]
    filt = None
    if len(sys.argv) > 2:
      filt = yaml.load("[%s]" % sys.argv[2])

    with open(fn,"r") as f:
      tasks = yaml.load(f.read())
      prefix = ""
      glo = {}
      if "__global__" in tasks:
        glo = tasks["__global__"]
      if "__prefix__" in tasks:
        prefix = tasks["__prefix__"]
      for k,v in tasks.iteritems():
        if k.startswith("__") and k.endswith("__"): continue #special
        if filt != None and k not in filt: continue
        for gk,gv in glo.iteritems():
          if gk not in v:
            v[gk] = gv
        v["destfile"] = prefix + k + ".mid"
        v["trkprefix"] = k
        mod_convert(**v)


